const express = require('express');
const router = express.Router();
const bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicleta_list_get);

router.post('/create', bicicletaController.bicicleta_post);

router.put('/:id/update', bicicletaController.bicicleta_put);

router.delete('/delete', bicicletaController.bicicleta_delete);

module.exports = router;