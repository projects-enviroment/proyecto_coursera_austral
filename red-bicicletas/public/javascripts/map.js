let map = L.map('main_map').setView([-34.598660, -58.436967], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
    maxZoom: 18,
    tileSize: 512,
    zoomOffset: -1,
}).addTo(map);

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result){
    result.bicicletas.forEach(bici => {
      L.marker(bici.ubicacion, {title: bici.id}).addTo(map)
    });
  }
})