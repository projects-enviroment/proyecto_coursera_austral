const Bicicleta = function(id, color, modelo, ubicacion){
  this.id = id,
  this.color = color,
  this.modelo = modelo,
  this.ubicacion = ubicacion
}

Bicicleta.prototype.toString = function () {
  return `id: ${this.id} | color: ${this.color}`;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
  Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
  let aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
  if (aBici)
    return aBici;
  else
    throw new Error(`No existe una bici con el id ${aBiciId}`);  
}

Bicicleta.removeById = function(aBiciId){
  for (let i = 0; i < Bicicleta.allBicis.length; i++) {
    if(Bicicleta.allBicis[i].id == aBiciId){
      Bicicleta.allBicis.splice(i, 1);
      break;
    }  
  }
}

// let a = new Bicicleta(1, 'Azul', 'Ruta', [-34.605091, -58.459335]);
// let b = new Bicicleta(2, 'Rojo', 'Montaña', [-34.593857, -58.442983]);

// Bicicleta.add(a);
// Bicicleta.add(b);

module.exports = Bicicleta;